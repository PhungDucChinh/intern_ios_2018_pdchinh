//
//  AboutViewCell.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class AboutViewCell: UITableViewCell {

    @IBOutlet weak var lblAbout: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblAbout.layer.cornerRadius = 7
        lblAbout.layer.borderWidth = 1
        lblAbout.layer.borderColor = mainColor.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
