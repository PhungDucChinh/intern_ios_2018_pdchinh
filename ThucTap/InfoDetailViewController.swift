//
//  InfoDetailViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/30/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class InfoDetailViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvDetail: UITextView!
    
    var titleName = ""
    var detailInfo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = titleName
        tvDetail.text = detailInfo
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "img_Background")!)

        let trueHeight : CGFloat = detailInfo.heightWithConstrainedWidth(300, font: UIFont.systemFont(ofSize: 14))!
        
        if trueHeight < tvDetail.frame.size.height{
            tvDetail.isScrollEnabled = false
        }else{
            tvDetail.isScrollEnabled = true
        }
                    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
