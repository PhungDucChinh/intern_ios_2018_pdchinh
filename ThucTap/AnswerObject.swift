//
//  AnswerObject.swift
//  ThucTap
//
//  Created by phungducchinh on 7/16/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

public class AnswerObject {
    public var idquest : Int?
    public var name : String?
    public var idkey : Int?
    
    init(idquest : Int, name : String, idkey : Int) {
        self.idquest = idquest
        self.name = name
        self.idkey = idkey
    }
}

public class ListAnswer {
    public var list = [AnswerObject]()
    init() {
    }
}
