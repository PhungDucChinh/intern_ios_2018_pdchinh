//
//  RegisterViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/30/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnStudent: UIButton!
    @IBOutlet weak var btnTeacher: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    var type = 0 // loai tai khoan
    override func viewDidLoad() {
        super.viewDidLoad()

        tfPassword.isSecureTextEntry = true
        btnRegister.layer.cornerRadius = 7
        btnStudent.addTarget(self, action: #selector(actionStudent), for: .touchDown)
        btnTeacher.addTarget(self, action: #selector(actionTeacher), for: .touchDown)
        btnRegister.addTarget(self, action: #selector(actionRegitser), for: .touchDown)
        tfFullName.delegate = self
        tfUserName.delegate = self
        tfPhoneNumber.delegate = self
        tfEmail.delegate = self
        tfPassword.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    func actionTeacher(){
        if type == 1{
            btnTeacher.setImage(UIImage(named: "img_btnTeacherNot"), for: .normal)
            btnStudent.setImage(UIImage(named: "img_btnStudentNot"), for: .normal)
            type = 0
        }else{
            btnTeacher.setImage(UIImage(named: "img_btnTeacher"), for: .normal)
            btnStudent.setImage(UIImage(named: "img_btnStudentNot"), for: .normal)
            type = 1
        }
    }
    
    func actionStudent() {
        if type == 2{
            btnStudent.setImage(UIImage(named: "img_btnStudentNot"), for: .normal)
            btnTeacher.setImage(UIImage(named: "img_btnTeacherNot"), for: .normal)
            type = 0
        }else{
            btnStudent.setImage(UIImage(named: "img_btnStudent"), for: .normal)
            btnTeacher.setImage(UIImage(named: "img_btnTeacherNot"), for: .normal)
            type = 2
        }
    }
    
    func actionRegitser(){
        self.view.endEditing(true)
        guard let text = tfFullName.text, !text.isEmpty else { return }
        print(text)
       // let fullname = tfFullName.text as! String ?? nil
    }

}
