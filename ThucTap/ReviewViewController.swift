//
//  ReviewViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    
    @IBOutlet weak var navReview: UINavigationItem!
    @IBOutlet weak var rightbtnReview: UIBarButtonItem!
    @IBOutlet weak var tbvReview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        tbvReview.delegate = self
        tbvReview.dataSource = self
        tbvReview.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReviewViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewViewCell") as! ReviewViewCell
        cell.lblTitle.text = nameReview[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { // se sua lai sau nay khi co api
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      
            let cell = tableView.cellForRow(at: indexPath) as! ReviewViewCell
            cell.lblTitle.textColor = .blue
            if indexPath.row == 3 {
                self.performSegue(withIdentifier: "ReviewToQuestion", sender: nil)
            }else{
                self.performSegue(withIdentifier: "ReviewToDeatilReview", sender: nil)
            }
        }
        
    }
}
