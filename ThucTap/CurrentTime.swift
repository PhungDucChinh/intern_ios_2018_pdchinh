//
//  CurrentTime.swift
//  ThucTap
//
//  Created by phungducchinh on 7/11/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

struct CurrentTime {
    static func currentTime() -> String {
        let date = Date()
//        let calendar = Calendar.current
//        let hour = calendar.component(.hour, from: date)
//        let minutes = calendar.component(.minute, from: date)
//        let day = calendar.component(.day, from: date)
//        let month = calendar.component(.month, from: date)
//        let year = calendar.component(.year, from: date)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        let str = timeFormatter.string(from: date)
        
        return str
    }
}
