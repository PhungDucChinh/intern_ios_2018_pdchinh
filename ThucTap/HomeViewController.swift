//
//  HomeViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var btnAbout: UIButton!
    @IBOutlet weak var btnOpenView: UIButton!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    
//    let btnLoginItem : UIButton = UIButton()
    var tagLogin = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "img_Background")!)
        self.navigationController?.navigationBar.barTintColor = mainColor

        self.navigationController?.navigationBar.topItem?.title = "Ân TOEIC"
        
        if defaultLogin.string(forKey: kUserDefaultkeyLogin) != nil{
            tagLogin = 1
            
        }
        
        if tagLogin == 0 {
            setupLoginButton()
        }else{
            setupLogoutButton()
        }
     
        
    }
    
    func setupLoginButton(){
        let btnLoginItem : UIButton = UIButton()
        btnLoginItem.setImage(UIImage(named: "user-512"), for: .normal)
        btnLoginItem.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnLoginItem.addTarget(self, action: #selector(actionGoLoginView), for: .touchDown)
       self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(customView: btnLoginItem)
    }
    
    
    func setupLogoutButton(){
        let btnLoginItem : UIButton = UIButton()
        btnLoginItem.setImage(UIImage(named: "logout"), for: .normal)
        btnLoginItem.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnLoginItem.addTarget(self, action: #selector(actionLogOff), for: .touchDown)
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(customView: btnLoginItem)    }
    
    func actionGoLoginView(){
        self.performSegue(withIdentifier: "HomeToLogin", sender: nil)
    }
    
    func actionLogOff(){
        print("logoff")
        self.showAlertLogOff(title: "Bạn có muốn đăng xuất?", message: "")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension HomeViewController {
    
    func showAlertLogOff(title: String, message: String)  {
        
        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Đăng xuất", style: .default, handler: actionAlertLogOff))
        alertController.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: actionCancel))
        topController.present(alertController, animated:true, completion:nil)
    }
    
    func actionAlertLogOff(action: UIAlertAction){
        defaultLogin.removeObject(forKey: kUserDefaultkeyLogin)
        defaultLogin.removeObject(forKey: kUserDefaultkeyPass)
        tagLogin = 0
        self.viewDidLoad()
    }
    
    func actionCancel(action: UIAlertAction){
        print("cancel")
    }

}
