//
//  DateOpenObject.swift
//  ThucTap
//
//  Created by Phung Duc Chinh on 7/19/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

public class DateOpenObject {
    public var id : Int?
    public var name : String?
    public var date : String?
    
    init(id : Int, name : String, date : String) {
        self.id = id
        self.name = name
        self.date = date
    }
}

public class ListDate {
    public var list = [DateOpenObject]()
    init() {
    }
}
