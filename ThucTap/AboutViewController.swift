//
//  AboutViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//
import Foundation
import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var navAbout: UINavigationBar!
    @IBOutlet weak var tbvAbout: UITableView!
    
    var number = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        tbvAbout.delegate = self
        tbvAbout.dataSource = self
        tbvAbout.isScrollEnabled = false
        tbvAbout.separatorStyle = UITableViewCellSeparatorStyle.none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AboutToInfo" {
            if let vc = segue.destination as? InfoDetailViewController{
                print(number)
                vc.titleName = nameOfAbout[number]
                vc.detailInfo = detailOfAbout[number]
            }
            
        }
    }
}

extension AboutViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameOfAbout.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutViewCell") as! AboutViewCell
        
        cell.lblAbout.text = nameOfAbout[indexPath.row]
        cell.lblAbout.layer.cornerRadius = 7
        cell.layer.cornerRadius = 7
        cell.layer.masksToBounds = true
        cell.contentView.layer.cornerRadius = 7
        cell.contentView.layer.masksToBounds = true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        number = indexPath.row
        self.performSegue(withIdentifier: "AboutToInfo", sender: self)
    }
}
