//
//  QuestionViewCell.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class QuestionViewCell: UITableViewCell {

    
    @IBOutlet weak var tfQuestion: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        tfQuestion.isEnabled = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
