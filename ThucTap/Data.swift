//
//  Data.swift
//  ThucTap
//
//  Created by phungducchinh on 6/30/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

let nameOfAbout = ["Giới thiệu về trung tâm Ân TOEIC", "Khoá học RESET - cơ bản", "Khoá học TOEIC - Intensive", "Khoá học TOEIC - Speaking - Writing"]

let detailOfAbout = ["Anh ngữ Ân TOEIC do thầy Hoàng Ân sáng lập với mục tiêu đem đến các khoá học “Hiệu quả nhất trong thời gian ngắn nhất”.\n\nTại ÂnTOEIC, đã nghiên cứu ra một chương trình học có thể giúp rút ngắn thời gian từ 2 đến 3 năm còn 6,5 tháng (nếu học đủ 3 khoá).\n\nCác khoá học được thiết kế tinh gọn, đúng trọng tâm và khoa học, tạo thành một hệ thống duy nhất chỉ có tại ÂnTOEIC, với nhiều phương pháp độc đáo được sáng tạo bởi thầy Hoàng Ân.",
                     "Khoá học RESET được thiết kế để giúp bạn lấy lại căn bản trong thời gian nhanh nhất: 1,5 tháng.\n\nSau khoá học, bạn sẽ:\n\t-  Tăng 800 từ vựng sơ cấp.\n\t-  Nắm vững 16 điểm ngữ pháp căn bản, thiết yếu nhất.\n\t-  Phát âm chuẩn 44 âm tiếng anh.\n\t-  Đọc hiểu hơn 20 văn bản sơ - trung cấp......\n\nThời lượng khoá học: 1,5 tháng. Mỗi tuần học 3 buổi, mỗi buổi 1,5 tiếng.",
                     "Khoá học giải đề TOEIC Intensive được thiết kế để giúp bạn đạt được số điểm mong muốn trong thời gian ngắn nhất có thể.\n\nSau khoá học, bạn sẽ:\n\t-  Tăng 200đ so với điểm đầu vào.\n\t-  Tăng 2000 từ TOEIC thiết yếu.\n\t-  Đọc hiểu 80% văn bản.\n\t-  Nghe hiểu 70%.\n\nThời lượng khoá học: 3 tháng. Mỗi tuần học 3 buổi, mỗi buổi 1,5 tiếng.",
                     "Khoá TOEIC SW là khoá cao hơn của khoá giải đề TOEIC, giúp bạn hoàn thiện cả 4 kỹ năng, phù hợp với các bạn giao tiếp trong công sở.\n\nSau khoá học, bạn sẽ:\n\t-  Phát âm chuẩn và ngữ điệu tốt.\n\t-  Nâng cao 150% phản xạ.\n\t-  Viết được các thể loại văn bản trong công việc.\n\t-  Giải thành thạo 6 đề thi SW chuẩn ETS độc quyền tại Ân TOEIC.\n\nThời lượng khoá học: 2 tháng. Mỗi tuần học 3 buổi, mỗi buổi 1,5 tiếng."]

let nameReview = ["Kinh nghiệm dự thi", "Cách học hiệu quả phần nghe", "Cách học hiệu quả phần đọc", "10 câu trắc nghiệm ngẫu nhiên"]
