//
//  LoginViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/30/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var tfUsernam: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

//        kLoginname = tfUsernam.text!
//        kpassword = tfPassword.text!
        
        tfUsernam.text = kLoginname
        tfPassword.text = kpassword
        
        
        btnLogin.layer.cornerRadius = 7
        btnLogin.addTarget(self, action: #selector(actionLogin), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginToHome" {
            if let vc = segue.destination as? HomeViewController{
                vc.tagLogin = 1
            }
            
        }
    }
    
    func actionLogin(){
        if (tfUsernam.text == kLoginname) && (tfPassword.text == kpassword) {
            defaultLogin.set(tfUsernam.text, forKey: kUserDefaultkeyLogin)
            defaultLogin.set(tfPassword.text, forKey: kUserDefaultkeyPass)
            print("login success")
            self.performSegue(withIdentifier: "LoginToHome",   sender: self)
        }
    }

}
