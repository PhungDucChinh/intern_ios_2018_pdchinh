//
//  LoadAlert.swift
//  ThucTap
//
//  Created by Phung Duc Chinh on 7/20/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

class LoadAlert : UIViewController{
    
    public static let instance = LoadAlert()
    let success = 0
    
    func showAlertDownload(title: String, message: String) {
        
        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        topController.present(alertController, animated:true, completion:nil)
        print("day la ham in ra alert")
    }
    
    func showAlertBack(title: String, message: String) {
        
        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: comBack))
        topController.present(alertController, animated:true, completion:nil)
        print("day la ham in ra alert")
    }
    
    func comBack(action: UIAlertAction){
        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        topController.performSegue(withIdentifier: "ProcessToHome", sender: nil)
    }
    
    func showAlertLogOff(title: String, message: String)  {
        
        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Đăng xuất", style: .default, handler: actionLogOff))
        alertController.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: actionCancel))
        topController.present(alertController, animated:true, completion:nil)
    }
    
    func actionLogOff(action: UIAlertAction){
        defaultLogin.removeObject(forKey: kUserDefaultkeyLogin)
        defaultLogin.removeObject(forKey: kUserDefaultkeyPass)
//        let vc = HomeViewController()
//        vc.tagLogin = 0
//        vc.viewDidLoad()
//        var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
//        while ((topController.presentedViewController) != nil) {
//            topController = topController.presentedViewController!;
//        }
//        topController.present(vc, animated:true, completion:nil)
    }
    
    func actionCancel(action: UIAlertAction){
        print("cancel")
    }

}
