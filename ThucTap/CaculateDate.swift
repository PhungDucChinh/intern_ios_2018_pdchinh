//
//  CaculateDate.swift
//  ThucTap
//
//  Created by phungducchinh on 6/10/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import Foundation
import UIKit

extension Date{
    static func caculateDate(day: Int, month: Int, year: Int, hour: Int , minute: Int) -> Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/mm/yyyy hh:mm"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 7)
        let caculateDate = formatter.date(from: "\(day)/\(month)/\(year) \(hour):\(minute)")
        return caculateDate!
    }
}
