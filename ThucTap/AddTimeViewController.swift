//
//  AddTimeViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class AddTimeViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txfTitle: UITextField!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var pkDate: UIDatePicker!
    @IBOutlet weak var btnAdd: UIButton!
    
    var strDate = ""
    var titleOpen = ""
    var date : DateOpenning?
    var id : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAdd.layer.cornerRadius = 7
        pkDate.datePickerMode = .dateAndTime
        txfTitle.layer.cornerRadius = 7
        pkDate.addTarget(self, action: #selector(handler(sender:)), for: UIControlEvents.valueChanged)
        btnAdd.addTarget(self, action: #selector(PostOpenningDate), for: .touchUpInside)
        txfTitle.addTarget(self, action: #selector(TextFieldChange), for: .editingChanged)
        txfTitle.delegate = self
        id = listOpen.list.count
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handler(sender: UIDatePicker){
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        strDate = timeFormatter.string(from: pkDate.date)
        self.lblTime.text = strDate
    }

    func PostOpenningDate(){
        let a = DateOpenObject(id: id, name: txfTitle.text!, date: lblTime.text!)
        id += 1
        listOpen.list.append(a)
        
        print(listOpen.list.count)
        
//        let alert = UIAlertController(title: "Openning Date ", message: strDate, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Accept", style: .default) { action in
//            print("da nhan thong tin")p
//        })
//        self.present(alert, animated: true, completion: nil)
        self.performSegue(withIdentifier: "AddOpenToListOpen", sender: nil)
    }

    func TextFieldChange(){
        self.titleOpen = txfTitle.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
}
