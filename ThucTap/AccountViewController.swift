//
//  AccountViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var navAccount: UINavigationItem!
    
    @IBOutlet weak var btnSeeInfo: UIButton!
    @IBOutlet weak var btnGotoRegister: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "img_Background")!)
        self.navigationController?.isNavigationBarHidden = true
        btnSeeInfo.layer.cornerRadius = 7
        btnGotoRegister.layer.cornerRadius = 7
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
