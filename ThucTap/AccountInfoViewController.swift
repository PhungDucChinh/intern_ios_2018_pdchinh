//
//  AccountInfoViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 7/1/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class AccountInfoViewController: UIViewController {

    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblTypeOfAccount: UILabel!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var btnEditAccount: UIBarButtonItem!
    
    var hide = 0 // 0 la khong show, 1 la show
    override func viewDidLoad() {
        super.viewDidLoad()

        lblPassword.text = "********"
        btnShowPassword.setImage(UIImage(named: "img_icon_show"), for: .normal)
        btnShowPassword.addTarget(self, action: #selector(actionShowPass), for: .touchDown)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionShowPass(){
        if hide == 0 {
            btnShowPassword.setImage(UIImage(named: "img_icon_Notshow"), for: .normal)
            lblPassword.text = "abcd12345"
            hide = 1
        }else{
            btnShowPassword.setImage(UIImage(named: "img_icon_show"), for: .normal)
            lblPassword.text = "********"
            hide = 0
        }
    }
    
    func actionEditAccount(){
        self.performSegue(withIdentifier: "SegueEditAccount", sender: nil)
    }

}
