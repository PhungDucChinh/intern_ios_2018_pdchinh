//
//  QuestionViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {

   
    @IBOutlet weak var tvQuestion: UITextView!
    @IBOutlet weak var tbvQuestion: UITableView!
 
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var answer = [AnswerObject]()
    var questtionRetun = ListQuest()
    var answerRetun = ListAnswer()
    let lblAnswer = ["A.", "B.", "C.","D."]
    var key = [""]
    var ques = [""]
    var count : Int = 0
    
    var index = IndexPath(row: 1, section: 1)
    
    var dem : Int = 0
    var da : Int = 0
    var check = [String](repeating: "", count: 10)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllQuestion()
        getAllAnswer()
        tbvQuestion.delegate = self
        tbvQuestion.dataSource = self
        tbvQuestion.isScrollEnabled = false
        tbvQuestion.separatorStyle = .none
        let a = getQuset(int : dem)
        tvQuestion.text = a.name
        answer = getAnswer(int : dem)
        print(dem)
        count = questtionRetun.list.count // tong so cau hoi
        check = [String](repeating: "", count: count)
        key = getAllKey()
        
        tvQuestion.isEditable = false
        tvQuestion.sizeToFit()
        btnBack.layer.cornerRadius = 7
        btnBack.addTarget(self, action: #selector(BackTarget), for: .touchUpInside)
        btnNext.layer.cornerRadius = 7
        btnNext.addTarget(self, action: #selector(NextTarget), for: .touchUpInside)
        btnSubmit.addTarget(self, action: #selector(Submit), for: .touchUpInside)
        btnSubmit.layer.cornerRadius = 7
        btnBack.isEnabled = true
        btnNext.isEnabled = true
        btnSubmit.isEnabled = true

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Submit(){
        da = 0
        
        for i in 0...9{
            print(check[i])
            if check[i] == key[i] , check[i] != ""{ // swift 3
                da += 1
            }
        }
        print("Your mark is " + "\(da)")
        let mark = String(da)
        let alert = UIAlertController(title: "Điểm của bạn là: ", message: mark, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            print("da nhan thong tin")
        })
        self.present(alert, animated: true, completion: nil)
    }
    
   func BackTarget() {
        print("back")
        index.section = 1
        dem = (dem - 1 + count) % count
        let a = getQuset(int : dem)
        tvQuestion.text = a.name
        answer = getAnswer(int : dem)
        tbvQuestion.reloadData()
        print("check when back: " + "\(check.count)")
    }
    
    func NextTarget() {
        print("next")
        index.section = 1
        dem = (dem + 1) % count
        let a = getQuset(int : dem)
        tvQuestion.text = a.name
        answer = getAnswer(int : dem)
        tbvQuestion.reloadData()
        print("check when next: " + "\(check.count)")
    }
    
    func getAllQuestion(){
        if let path = Bundle.main.path(forResource: "data_credit", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult : Array = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [Dictionary<String, Any>]
                
                for i in jsonResult{
                    
                    let questData = QuestObject(idquest: i["idquest"] as! Int, name: i["name"] as! String, idkey: i["idkey"] as! Int)
                    self.questtionRetun.list.append(questData)
                    
                }
                print(self.questtionRetun.list.count)
            } catch {
                // handle error
            }
        }
    }
    
    func getAllAnswer(){
        if let path = Bundle.main.path(forResource: "data_answer", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult : Array = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! [Dictionary<String, Any>]
                
                for i in jsonResult{
                    let answerData = AnswerObject(idquest: i["idques"] as! Int, name: i["name"] as! String, idkey: i["idkey"] as! Int)
                    answerRetun.list.append(answerData)
                }
                print(answerRetun.list.count)
            } catch {
                // handle error
            }
        }
    }
    
    func getAnswer(int : Int) -> [AnswerObject]{
        var arr = [AnswerObject]()
        for i in answerRetun.list{
            if i.idquest == int{
                arr.append(i)
            }
        }
        print(arr.count)
        return arr
    }
    
    func getQuset(int : Int) -> QuestObject{
        var arr = QuestObject(idquest: int, name: "", idkey: int)
        for i in questtionRetun.list{
            if i.idquest == int{
                arr = i
            }
        }
        return arr
    }
    

    func getAllKey() -> [String]{
        var arr = [String]()
    
        for i in 0...questtionRetun.list.count-1{
            let arrAnwser = getAnswer(int: i)
            arr.append(arrAnwser[questtionRetun.list[i].idkey! - 1].name!)
            print(arr[i])
        }
        
        
        return arr
    }
}

extension QuestionViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionViewCell") as! QuestionViewCell
        let ans = answer[indexPath.row]
        if indexPath == index || answer[indexPath.row].name == check[dem]{
            cell.tfQuestion.layer.borderWidth = 2
            cell.tfQuestion.layer.borderColor = UIColor.red.cgColor
            cell.tfQuestion.layer.cornerRadius = 7
            cell.tfQuestion.text = lblAnswer[indexPath.row] + " " + ans.name!
            cell.tfQuestion.textColor = UIColor.white
            cell.tfQuestion.backgroundColor = UIColor.red
            cell.selectionStyle = .none
            cell.tfQuestion.setLeftPaddingPoints(10)
            //cell.backgroundColor = UIColor.red
        }else{
            cell.tfQuestion.text = lblAnswer[indexPath.row] + " " + ans.name!
            cell.tfQuestion.layer.borderWidth = 2
            cell.tfQuestion.layer.borderColor = UIColor.green.cgColor
            cell.tfQuestion.layer.cornerRadius = 7
            cell.tfQuestion.textColor = UIColor.black
            cell.tfQuestion.backgroundColor = UIColor.white
            cell.selectionStyle = .none
            cell.tfQuestion.setLeftPaddingPoints(10)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    
        index = indexPath
   
        check.remove(at: dem)
        check.insert(answer[indexPath.row].name!,at: dem)
        print("check when click: " + "\(check.count)")
        print("chose" + check[dem])
        tbvQuestion.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { // change 2 to desired number of seconds
            self.NextTarget()
            self.btnBack.isEnabled = false
            self.btnNext.isEnabled = false
            self.btnSubmit.isEnabled = false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}

