//
//  OpenViewController.swift
//  ThucTap
//
//  Created by phungducchinh on 6/14/18.
//  Copyright © 2018 phungducchinh. All rights reserved.
//

import UIKit

class OpenViewController: UIViewController {
    @IBOutlet weak var navOpen: UINavigationItem!

    @IBOutlet weak var rightbtnAdd: UIBarButtonItem!
    
    @IBOutlet weak var tbvOpen: UITableView!
    @IBOutlet weak var navOpenBar: UINavigationBar!
    
    var list = ListDateOpenning.list

    let current = CurrentTime.currentTime()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        
        tbvOpen.delegate = self
        tbvOpen.dataSource = self
//        tbvOpen.separatorStyle = .none
        print(current)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OpenViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOpen.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OpenViewCell") as! OpenViewCell
        let date = listOpen.list[indexPath.row]
        
        cell.lblTitle.text = date.name
        cell.lblTime.text = "Khai giảng vào ngày " + date.date!
        
        if current < date.date! {
            print("sap den ngay")
            cell.imgNew?.isHidden = false
            cell.imgNew.image = UIImage(named: "new_icon")
        }else{
            print("da qua ngay")
            cell.imgNew?.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OpenViewCell
        cell.lblTitle.textColor = .blue
        cell.lblTime.textColor = .blue
        self.performSegue(withIdentifier: "OpenToContact", sender: nil)
    }
}
